import 'package:flutter/material.dart';

const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);
const kDefaultPadding = 20.0;
const double kAppBarButtonSplashRadius = 20.0;
const kButtonDarkColor = Color(0xFF535353);
const kButtonLightColor = Color(0xFFFFFFFF);

class Constants {
  static Color color = Colors.white;
  static MediaQueryData? _mediaQueryData;
  static double? screenWidth;
  static double? screenHeight;
  static double? appBarHeight;

  static const TextStyle boldTextStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 18);

  static const TextStyle mediuTextStyle =
      TextStyle(fontWeight: FontWeight.normal, fontSize: 14);

  static const TextStyle smallTextStyle =
      TextStyle(fontWeight: FontWeight.w200, fontSize: 12);

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData?.size.width;
    screenHeight = _mediaQueryData?.size.height;
    appBarHeight = screenHeight! * 0.2;
    /* blockSizeHorizontal = (screenWidth! / 3);
    blockSizeVertical = (screenHeight! / 3);
    blockSize = (screenWidth! / 5);
    blockSizePadding = (screenWidth! / 50);*/
  }
}
