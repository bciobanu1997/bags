import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/product_data.dart';

class BottomSheetBagDetails extends StatelessWidget {
  const BottomSheetBagDetails({Key? key, this.product}) : super(key: key);
  final Product? product;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Colors.grey.shade700, Colors.white],
      )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Collection',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                Text('${product!.title}',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.w600))
              ],
            ),
          ),
          Stack(
            children: [
              /*Container(
                margin: EdgeInsets.only(top: size.height * 0.203),
                width: size.width,
                height: size.height / 4,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(24),
                    topRight: Radius.circular(24),
                  ),
                ),
              ),*/
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Column(
                      children: [
                        Text('Price: ',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                        Text(
                          '\$${product!.price}',
                          style: TextStyle(color: Colors.white, fontSize: 35),
                        )
                      ],
                    ),
                  ),
                  Expanded(child: Image.asset(product!.image)),
                ],
              ),
            ],
          ),
          SizedBox(height: 30),
        ],
      ),
    );
  }
}
