import 'package:bags/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../data/items_sumController.dart';

class CartAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CartAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var checkoutController = Get.put(SumController());
    return AppBar(
      leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.chevron_left,
            color: kTextColor,
          )),
      elevation: 0,
      backgroundColor: Color(0xF6F9FBFD),
      title: Column(
        children: [
          Text(
            'Cart',
            style: TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.w600,
              color: kTextColor,
            ),
          ),
          Obx(() => Text(
                '${checkoutController.bagItemCount.value} items',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  color: kTextColor,
                ),
              )),
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.square(50);
}
