import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../cart/cart_screen.dart';
import '../../constants/constant.dart';

class FullAppBar extends StatelessWidget implements PreferredSizeWidget {
  const FullAppBar({Key? key, required this.appBarColor}) : super(key: key);
  final Color appBarColor;
  @override
  Widget build(BuildContext context) {
    final Color color;
    MediaQueryData? _mediaQueryData = MediaQuery.of(context);
    double screenWidth = _mediaQueryData.size.width;
    double screenHeight = _mediaQueryData.size.height;
    double appBarHeight = screenHeight * 0.1;

    return AppBar(
      shadowColor: Colors.transparent,
      backgroundColor: appBarColor,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(Icons.arrow_back),
        splashRadius: kAppBarButtonSplashRadius,
        color: kButtonLightColor,
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.search),
          splashRadius: kAppBarButtonSplashRadius,
          color: kButtonLightColor,
        ),
        IconButton(
          onPressed: () {
            Navigator.push(
              context,
              CupertinoPageRoute(builder: (context) => CartScreen()),
            );
          },
          icon: Icon(Icons.shopping_cart_outlined),
          splashRadius: kAppBarButtonSplashRadius,
          color: kButtonLightColor,
        ),
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.square(50);
}
