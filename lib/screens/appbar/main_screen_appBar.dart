import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants/constant.dart';

class MainScreenAppBar extends StatefulWidget implements PreferredSizeWidget {
  MainScreenAppBar({Key? key}) : super(key: key);
  @override
  State<MainScreenAppBar> createState() => MainScreenAppBarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.square(50);
}

class MainScreenAppBarState extends State<MainScreenAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.search),
          splashRadius: kAppBarButtonSplashRadius,
          color: kButtonDarkColor,
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.shopping_cart_outlined),
          splashRadius: kAppBarButtonSplashRadius,
          color: kButtonDarkColor,
        )
      ],
      backgroundColor: Colors.white,
      elevation: 0,
    );
  }
}
