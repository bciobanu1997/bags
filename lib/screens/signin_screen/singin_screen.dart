import 'package:bags/getx_helper/firebase_controller.dart';
import 'package:bags/png_images/png_images.dart';
import 'package:bags/screens/register_screen/register_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../constants/constant.dart';
import '../../data/product_data.dart';

class SignInScreen extends GetWidget<FirebaseController> {
  SignInScreen({Key? key}) : super(key: key);
  TextEditingController email = TextEditingController();
  TextEditingController pass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 100,
            width: 100,
            child: Image(image: AssetImage(PngImages.bag4)),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              keyboardAppearance: Brightness.dark,
              keyboardType: TextInputType.emailAddress,
              controller: email,
              decoration: InputDecoration(
                hintText: 'Email',
                hintStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                  borderSide: BorderSide(color: products[4].color),
                ),
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              ),
            ),
          ),
          SizedBox(height: 15),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              controller: pass,
              decoration: InputDecoration(
                hintText: 'Password',
                hintStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                  borderSide: BorderSide(color: products[4].color),
                ),
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              ),
            ),
          ),
          SizedBox(height: 15),
          ElevatedButton(
              onPressed: () {
                loginUser();
              },
              style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  )),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(products[3].color)),
              child: Text(
                'Log in',
                style: TextStyle(),
              )),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('New User?'),
              TextButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterScreen()));
                  },
                  child: Text('Register Now'))
            ],
          )
        ],
      ),
    ));
  }

  void loginUser() {
    controller.login(email.text, pass.text);
  }
}
