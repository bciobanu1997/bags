import 'package:flutter/cupertino.dart';

class MainScreenHeader extends StatelessWidget {
  const MainScreenHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: const [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Text(
                'Womens Bags',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
