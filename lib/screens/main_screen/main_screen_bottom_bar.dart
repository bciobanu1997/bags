import 'package:bags/getx_helper/firebase_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants/constant.dart';

class BottomBodyScreen extends GetWidget<FirebaseController> {
  const BottomBodyScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {},
                child: const Text('Contants',
                    style: TextStyle(color: kTextColor))),
            TextButton(
                onPressed: () {},
                child: const Text('Information',
                    style: TextStyle(color: kTextColor))),
            TextButton(
                onPressed: () {},
                child: const Text('News',
                    style: const TextStyle(color: kTextColor))),
            TextButton(
                onPressed: () {},
                child: const Text('About',
                    style: const TextStyle(color: kTextColor))),
            TextButton(
                onPressed: () {
                  controller.signout();
                },
                child: const Text('Sign Out',
                    style: const TextStyle(color: Color(0xFFAF0E00)))),
          ],
        ),
      ],
    );
  }
}
