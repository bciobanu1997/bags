import 'package:flutter/material.dart';

import 'main_screen_body.dart';
import 'main_screen_bottom_bar.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: const [
            SizedBox(height: 10),
            ProductItems(),
            BottomBodyScreen(),
          ],
        ),
      ),
    );
  }
}
