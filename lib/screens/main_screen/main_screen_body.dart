import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../cart/cart_screen.dart';
import '../../constants/constant.dart';
import '../../data/items_sumController.dart';
import '../../data/product_data.dart';
import '../bag_details_screen/bag_details_screen.dart';
import '../bottom_sheet_bag_details_screen/bottom_sheet_bag_details.dart';
import 'main_screen_product_container.dart';

class ProductItems extends StatefulWidget {
  const ProductItems({Key? key}) : super(key: key);

  @override
  State<ProductItems> createState() => _ProductItemsState();
}

class _ProductItemsState extends State<ProductItems> {
  final checkoutController = Get.put(SumController());
  final searchController = TextEditingController();
  bool isSearching = false;

  var isVisible = true;

  late ScrollController _controller;

  void searchBags() {
    final query = searchController.text;

    if (query.isNotEmpty) {
      filteredBag = products.where((Product products) {
        return products.title.toLowerCase().contains(query.toLowerCase());
      }).toList();
    } else {
      filteredBag = products;
    }
    setState(() {});
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        isVisible = false;
        isSearching = false;
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        isSearching = true;
        isVisible = true;
      });
    }
  }

  @override
  void initState() {
    searchBags();
    searchController.addListener(searchBags);
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 250,
                height: 40,
                child: (isSearching == false)
                    ? Text('')
                    : Visibility(
                        visible: isVisible,
                        child: TextField(
                          controller: searchController,
                          decoration: InputDecoration(
                            labelText: 'Search',
                            filled: true,
                            fillColor: Colors.white.withAlpha(235),
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    this.isSearching = !this.isSearching;
                    isVisible = true;
                  });
                },
                icon: Icon(Icons.search),
                splashRadius: kAppBarButtonSplashRadius,
                color: kButtonDarkColor,
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const CartScreen()),
                  );
                },
                icon: Icon(Icons.shopping_cart_outlined),
                splashRadius: kAppBarButtonSplashRadius,
                color: kButtonDarkColor,
              )
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: 50),
            child: GridView.builder(
              controller: _controller,
              shrinkWrap: true,
              itemCount: filteredBag.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 0.8),
              itemBuilder: (context, index) => ItemCard(
                product: filteredBag[index],
                press: () {
                  Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => BagDetailsScreen(
                        product: filteredBag[index],
                      ),
                    ),
                  );
                },
                longPress: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (context) => BottomSheetBagDetails(
                      product: filteredBag[index],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
