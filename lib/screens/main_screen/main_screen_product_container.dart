import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/product_data.dart';

class ItemCard extends StatelessWidget {
  final Product? product;
  final VoidCallback? press;
  final VoidCallback? longPress;

  const ItemCard({Key? key, required this.product, this.press, this.longPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: longPress,
      onTap: press,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 160,
              height: 180,
              decoration: BoxDecoration(
                color: product?.color,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Container(child: Image.asset(product!.image)),
            ),
            Text(
              product!.title,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
            ),
            Text(
              "\$${product?.price}",
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }
}
