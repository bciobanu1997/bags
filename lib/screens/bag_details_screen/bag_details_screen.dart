import 'package:bags/screens/appbar/full_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../constants/constant.dart';
import '../../data/items_sumController.dart';
import '../../data/product_data.dart';

class BagDetailsScreen extends StatefulWidget {
  const BagDetailsScreen({Key? key, required this.product}) : super(key: key);
  final Product product;

  @override
  State<BagDetailsScreen> createState() => _BagDetailsScreenState();
}

class _BagDetailsScreenState extends State<BagDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    final checkoutController = Get.put(SumController());
    _showSnackBar(GlobalKey<ScaffoldState> _scaffoldKey) {
      final snackBar = SnackBar(
        content: SizedBox(
          child: Container(
              child: Center(
                  child: Text(
                "Connection dropped.",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              )),
              height: 1.0),
        ),
        duration: Duration(seconds: 10),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: widget.product.color,
      appBar: FullAppBar(appBarColor: widget.product.color),
      body: SizedBox(
        height: size.height,
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: size.height * 0.35),
              padding:
                  EdgeInsets.only(top: size.height * 12, left: kDefaultPadding),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24),
                  topRight: Radius.circular(24),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 20, left: 10),
                  child: Text('Collection',
                      style: TextStyle(fontSize: 20, color: Colors.white)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    '${widget.product.title}',
                    style: const TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Price',
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                        Text(
                          '\$${widget.product.price}',
                          style: TextStyle(fontSize: 40, color: Colors.white),
                        ),
                      ],
                    ),
                    Container(
                      width: 250,
                      height: 300,
                      child: Image.asset(widget.product.image),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Color',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w400),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              ColorBagDot(
                                  color: widget.product.color,
                                  isSelected: true),
                              ColorBagDot(color: Colors.amber),
                              ColorBagDot(color: Colors.green.shade400),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 20),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('Size',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w400)),
                          Row(
                            children: [
                              Text(
                                '${widget.product.size} cm',
                                style: const TextStyle(
                                    fontSize: 24, fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 20),
                    OutlinedButton(
                      onPressed: () {
                        cartProducts.add(widget.product);
                        checkoutController.sumItem(cartProducts);
                        print('${widget.product.id}');
                        /* var snackBar = SnackBar(
                            content: Container(
                                height: 10, child: Text('Hello World')));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);*/
                        Get.snackbar('', '',
                            snackStyle: SnackStyle.FLOATING,
                            padding: EdgeInsets.only(top: 20),
                            titleText: Text('Item was added'),
                            icon: Icon(Icons.file_download_done),
                            duration: Duration(milliseconds: 800),
                            maxWidth: 200,
                            backgroundColor: Colors.grey.shade50,
                            snackPosition: SnackPosition.BOTTOM);
                      },
                      child: Icon(
                        Icons.add_shopping_cart,
                        size: 35,
                      ),
                      style: OutlinedButton.styleFrom(
                        primary: widget.product.color,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                        ),
                        side: BorderSide(color: Colors.transparent),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Description',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600)),
                      SizedBox(height: 10),
                      Text('${widget.product.description}'),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ColorBagDot extends StatelessWidget {
  final Color color;
  final bool isSelected;
  const ColorBagDot({
    Key? key,
    required this.color,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5, right: 5),
      padding: EdgeInsets.all(1),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: isSelected ? color : Colors.transparent)),
      height: 25,
      width: 25,
      child: DecoratedBox(
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
      ),
    );
  }
}
