import 'package:bags/constants/constant.dart';
import 'package:bags/data/product_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../getx_helper/firebase_controller.dart';
import '../../png_images/png_images.dart';
import '../signin_screen/singin_screen.dart';

class RegisterScreen extends GetWidget<FirebaseController> {
  RegisterScreen({Key? key}) : super(key: key);
  String passPattern =
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  String namePatternNum = r'(?=.*?[0-9]).{0,}$';
  String namePatternSymbol = r'(?=.*?[!@#\$&*~]).{0,}$';
  String namePatter = r'^(?=.*?[A-Z])(?=.*?[a-z]).{0,}$';
  final TextEditingController firstn = TextEditingController();
  final TextEditingController lastn = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 100,
            width: 100,
            child: Image(image: AssetImage(PngImages.bag4)),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              textCapitalization: TextCapitalization.sentences,
              controller: firstn,
              decoration: InputDecoration(
                hintText: 'First Name',
                hintStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                  borderSide: BorderSide(color: products[4].color),
                ),
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              ),
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              textCapitalization: TextCapitalization.sentences,
              controller: lastn,
              decoration: InputDecoration(
                hintText: 'Last Name',
                hintStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                  borderSide: BorderSide(color: products[4].color),
                ),
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              ),
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              controller: email,
              decoration: InputDecoration(
                hintText: 'Email',
                hintStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                  borderSide: BorderSide(color: products[4].color),
                ),
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              ),
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: TextField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              controller: password,
              decoration: InputDecoration(
                hintText: 'Password',
                hintStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10),
                  borderSide: BorderSide(color: products[4].color),
                ),
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              ),
            ),
          ),
          SizedBox(height: 10),
          ElevatedButton(
              onPressed: () {
                registerUser();
              },
              style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  )),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(products[3].color)),
              child: Text(
                'Sign-Up',
                style: TextStyle(),
              )),
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Existing User?'),
              TextButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SignInScreen()));
                    /*Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SignInScreen()),
                    );*/
                  },
                  child: Text('Login Now'))
            ],
          )
        ],
      ),
    ));
  }

  void registerUser() {
    if (!RegExp(passPattern).hasMatch(password.text)) {
      Get.snackbar('Password',
          'Password needs to be 8-16 characters, one uppercase, one lowercase, one special character,',
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(milliseconds: 900),
          backgroundColor: Colors.grey.shade100);
    } else if (!RegExp(namePatter).hasMatch(firstn.text) &&
        (!RegExp(namePatter).hasMatch(lastn.text))) {
      Get.snackbar('Incorect first name or first name',
          'Last name and first name cant contain symbols or numbers',
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(milliseconds: 900),
          backgroundColor: Colors.grey.shade100);
    } else if (RegExp(namePatternNum).hasMatch(firstn.text) ||
        RegExp(namePatternSymbol).hasMatch(firstn.text)) {
      Get.snackbar('Incorect first name or first name',
          'Last name and first name cant contain symbols or numbers',
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(milliseconds: 900),
          backgroundColor: Colors.grey.shade100);
    } else if (RegExp(namePatternNum).hasMatch(lastn.text) ||
        RegExp(namePatternSymbol).hasMatch(lastn.text)) {
      Get.snackbar('Incorect first name or first name',
          'Last name and first name cant contain symbols or numbers',
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(milliseconds: 900),
          backgroundColor: Colors.grey.shade100);
    } else {
      Get.snackbar('Succes', 'Registration confirmed,',
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(milliseconds: 900),
          backgroundColor: Colors.white);
      controller.createUser(
          firstn.text,
          lastn.text,
          email.text,
          password
              .text); // Then is correct navigate to a page throught Get.offAll()
    }
  }
}
