import 'dart:ui';

class Product {
  String image, title, description;
  int price, size, id;
  Color color;

  Product({
    required this.image,
    required this.title,
    required this.description,
    required this.price,
    required this.size,
    required this.id,
    required this.color,
  });

  /*int sum() {
    int sum = 0;
    for (int i = 0; i < cartProducts.length; i++) {
      sum += cartProducts[i].price;
    }
    return sum;
  }*/
}

List<Product> cartProducts = [];
List<Product> filteredBag = [];
List<Product> products = [
  Product(
      image: "assets/images/bag1.png",
      title: "Spring Bag",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 300,
      size: 8,
      id: 1,
      color: const Color(0xFFAF6641)),
  Product(
      image: "assets/images/bag2.png",
      title: "Gucci Vermucci",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 200,
      size: 8,
      id: 2,
      color: const Color(0xFF903C3F)),
  Product(
      image: "assets/images/bag3.png",
      title: "Gabbana Dolce",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 400,
      size: 8,
      id: 3,
      color: const Color(0xFF7B7970)),
  Product(
      image: "assets/images/bag5.png",
      title: "Mikel Corso",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 3000,
      size: 8,
      id: 4,
      color: const Color(0xFF404453)),
  Product(
      image: "assets/images/bag6.png",
      title: "Armani Smartani",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 1000,
      size: 8,
      id: 5,
      color: const Color(0xFF82664C)),
  Product(
      image: "assets/images/bag7.png",
      title: "Velluro Pelusso",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 300,
      size: 8,
      id: 6,
      color: const Color(0xFF766660)),
  Product(
      image: "assets/images/bag8.png",
      title: "Purro Bellisimo",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 200,
      size: 8,
      id: 7,
      color: const Color(0xFF903C3F)),
  Product(
      image: "assets/images/bag3.png",
      title: "Mikel Corso",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 400,
      size: 8,
      id: 8,
      color: const Color(0xFF767265)),
  Product(
      image: "assets/images/bag1.png",
      title: "Gucci Vermucci",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 3000,
      size: 8,
      id: 9,
      color: const Color(0xFFAF6641)),
  Product(
      image: "assets/images/bag5.png",
      title: "Armani Smartani",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 1000,
      size: 8,
      id: 10,
      color: const Color(0xFF404453)),
  Product(
      image: "assets/images/bag5.png",
      title: "Armani Smartani",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 1000,
      size: 8,
      id: 11,
      color: const Color(0xFF404453)),
  Product(
      image: "assets/images/bag5.png",
      title: "Armani Smartani",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 1000,
      size: 8,
      id: 12,
      color: const Color(0xFF404453)),
  Product(
      image: "assets/images/bag5.png",
      title: "Armani Smartani",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 1000,
      size: 8,
      id: 13,
      color: const Color(0xFF404453)),
  Product(
      image: "assets/images/bag5.png",
      title: "Armani Smartani",
      description:
          "Green crocheted shoulder bag made of acrylic wool with a lining of silk (changeant) fabric. The bag closes with a zipper and on the inside is a pocket for small items. Size is approximately 30 x 30 cm. Handle is approximately 60 cm long. All bags are unique, there’s only one of them.",
      price: 1000,
      size: 8,
      id: 14,
      color: const Color(0xFF404453)),
];
