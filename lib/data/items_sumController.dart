import 'package:get/get.dart';

class SumController extends GetxController {
  RxInt total = 0.obs;
  RxInt bagItemCount = 0.obs;

  RxBool resiz = false.obs;
  sumItem(List cartProduct) {
    RxInt sum = 0.obs;
    for (int i = 0; i < cartProduct.length; i++) {
      sum += cartProduct[i].price;
    }
    bagItemCount.value = cartProduct.length;
    total.value = sum.value;
    return sum;
  }
}
