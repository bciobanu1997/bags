import 'package:bags/getx_helper/firebase_controller.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../screens/main_screen/main_screen.dart';
import '../screens/signin_screen/singin_screen.dart';

class SignedinUser extends GetWidget<FirebaseController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Get.find<FirebaseController>().user != null
          ? MainScreen()
          : SignInScreen();
    });
  }
}
