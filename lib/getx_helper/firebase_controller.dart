import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

import '../screens/main_screen/main_screen.dart';
import '../screens/signin_screen/singin_screen.dart';

class FirebaseController extends GetxController {
  FirebaseAuth _auth = FirebaseAuth.instance;
  Rxn<User> _firebaseUser = Rxn<User>();
  String? get user => _firebaseUser.value?.email;

  @override
  void onInit() {
    // TODO: implement onInit
    _firebaseUser.bindStream(_auth.authStateChanges());
  }

  //Function to create user, login and sign out user

  void createUser(
      String firstname, String lastname, String email, String password) async {
    CollectionReference reference =
        FirebaseFirestore.instance.collection("Users");
    Map<String, String> userdata = {
      "First Name": firstname,
      "Last Name": lastname,
      "Email": email
    };

    await _auth
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) => {
              reference
                  .add(userdata)
                  .then((value) => Get.offAll(SignInScreen()))
            }) // loginPage need
        .catchError((onError) => Get.snackbar(
            'Error while creating account', onError.message,
            snackPosition: SnackPosition.BOTTOM));
  }

  void login(String email, String password) async {
    await _auth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) => Get.offAll(MainScreen()))
        .catchError((onError) => Get.snackbar(
            'Error while sign in ', onError.message,
            snackPosition: SnackPosition
                .BOTTOM)); // Then is correct navigate to a page throught Get.offAll()
  }

  void signout() async {
    await _auth.signOut().then((value) => Get.offAll(SignInScreen()));
  }
}
