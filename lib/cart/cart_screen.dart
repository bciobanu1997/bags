import 'package:flutter/material.dart';

import '../data/product_data.dart';
import '../screens/appbar/cart_appbar.dart';
import 'cart_body.dart';
import 'cart_checkout.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key, this.product}) : super(key: key);
  final Product? product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CartAppBar(),
      body: CartBody(),
      bottomNavigationBar: CheckoutCart(),
    );
  }
}
