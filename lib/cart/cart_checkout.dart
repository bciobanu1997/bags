import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../data/items_sumController.dart';
import '../data/product_data.dart';

class CheckoutCart extends StatelessWidget {
  CheckoutCart({
    Key? key,
    this.product,
  }) : super(key: key);
  final Product? product;
  SumController checkoutController = Get.put(SumController());

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        height: 180,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30)),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, -15),
                blurRadius: 20,
                color: Color(0xFFDADADA).withOpacity(0.4),
              )
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xDEDEE0DB),
                      borderRadius: BorderRadius.circular(10)),
                  width: 40,
                  height: 40,
                  child: Icon(
                    Icons.receipt,
                    color: Colors.deepOrangeAccent,
                  ),
                ),
                SizedBox(
                  width: 250,
                  height: 45,
                  child: TextField(
                    maxLines: 1,
                    decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                              color: Colors.grey.shade100, width: 1.0),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                              color: Colors.grey.shade100, width: 1.0),
                        ),
                        suffixIcon: Icon(
                          Icons.chevron_right,
                          color: Colors.grey,
                        ),
                        hintText: 'Enter Voucher Code'),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: 50,
                  width: 50,
                  child: Obx(
                      () => Text('Total\n\$${checkoutController.total.value}')),
                ),
                SizedBox(
                  width: 250,
                  height: 45,
                  child: ElevatedButton(
                    onPressed: () {
                      /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const WhitePage()),
                      );*/
                    },
                    child: Text('Buy Now'),
                    style: ElevatedButton.styleFrom(
                      primary: (cartProducts.isEmpty)
                          ? Colors.deepOrange
                          : cartProducts.last.color,
                      onPrimary: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }
}

/*
Container(
width: width,
child: Column(
mainAxisAlignment: MainAxisAlignment.spaceEvenly,
children: [
Container(
decoration: BoxDecoration(
color: Colors.grey.shade100,
borderRadius: BorderRadius.circular(10)),
height: 50,
width: 50,
child: Icon(Icons.receipt,
color: Colors.deepOrangeAccent, size: 30)),
Container(
width: width,
height: 100,
child: Obx(
() => Text((cartProducts.isEmpty)
? '\$ 0'
: '\$${checkoutController.total.value = sumItem()}'),
),
)
],
),
),
Column(
mainAxisAlignment: MainAxisAlignment.spaceEvenly,
children: [
Container(
width: 250,
height: 50,
child: TextField(
maxLines: 1,
decoration: InputDecoration(
focusedBorder: OutlineInputBorder(
borderRadius: BorderRadius.circular(20),
borderSide: BorderSide(
color: Colors.grey.shade100, width: 1.0),
),
border: OutlineInputBorder(
borderRadius: BorderRadius.circular(20),
borderSide: BorderSide(
color: Colors.grey.shade100, width: 1.0),
),
suffixIcon: Icon(
Icons.chevron_right,
color: Colors.grey,
),
hintText: 'Enter Voucher Code'),
),
),
Container(
width: 250,
height: 50,
child: ElevatedButton(
onPressed: () {
print(cartProducts.asMap().toString());
},
child: Text('Buy Now'),
style: ElevatedButton.styleFrom(
primary: Colors.deepOrange,
onPrimary: Colors.white,
shape: RoundedRectangleBorder(
borderRadius: BorderRadius.circular(32.0),
),
),
),
)
],
)*/
