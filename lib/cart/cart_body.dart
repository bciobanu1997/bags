import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../data/items_sumController.dart';
import '../data/product_data.dart';
import 'cart_items.dart';

class CartBody extends StatefulWidget {
  CartBody({Key? key}) : super(key: key);

  @override
  State<CartBody> createState() => _CartBodyState();
}

class _CartBodyState extends State<CartBody> {
  final checkoutController = Get.put(SumController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: (cartProducts.isEmpty)
          ? Center(child: Text('Empty Bag'))
          : ListView.builder(
              itemCount: cartProducts.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Dismissible(
                  direction: DismissDirection.endToStart,
                  key: UniqueKey(),
                  background: Container(
                    decoration: BoxDecoration(
                        color: Color(0xFFFFE6E6),
                        borderRadius: BorderRadius.circular(15)),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        Spacer(),
                        Icon(
                          Icons.delete_outline,
                          color: Color(0xFFF1B1B1),
                        ),
                      ],
                    ),
                  ),
                  onDismissed: (dismiss) {
                    print(cartProducts);
                    cartProducts.removeAt(index);
                    checkoutController.sumItem(cartProducts);
                    print('Ondismiss total:${checkoutController.total}');
                  },
                  child: CartItem(cartProducts: cartProducts[index]),
                ),
              ),
            ),
    );
  }
}
