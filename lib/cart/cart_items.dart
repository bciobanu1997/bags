import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../data/product_data.dart';

class CartItem extends StatelessWidget {
  const CartItem({
    Key? key,
    required this.cartProducts,
  }) : super(key: key);
  final Product cartProducts;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 100,
          height: 100,
          child: AspectRatio(
            aspectRatio: 0.88,
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Color(0xFFF5F6F9),
                  borderRadius: BorderRadius.circular(15)),
              child: Image.asset(cartProducts.image),
            ),
          ),
        ),
        SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              cartProducts.title,
              style: TextStyle(fontSize: 16, color: Colors.black),
              maxLines: 2,
            ),
            SizedBox(height: 10),
            Text.rich(TextSpan(
                children: [TextSpan(text: ' x 1')],
                text: '\$${cartProducts.price}',
                style:
                    TextStyle(fontWeight: FontWeight.w600, color: Colors.red)))
          ],
        )
      ],
    );
  }
}
