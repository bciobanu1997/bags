import 'package:bags/screens/main_screen/main_screen.dart';
import 'package:flutter/cupertino.dart';

var customRoutes = <String, WidgetBuilder>{
  '/main': (context) => MainScreen(),
};
