//
//  Gradient.swift
//  Runner
//
//  Created by Boris on 01/04/2022.
//

import UIKit
@IBDesignable
class Gradient: UIView{
    
    @IBInspectable var topColor: UIColor = #colorLiteral(red: 0.9010884166, green: 0.706099689, blue: 0.5782302022, alpha: 1)
    @IBInspectable var bottomColor: UIColor = #colorLiteral(red: 0.9979020953, green: 0.6928688288, blue: 0.820553124, alpha: 1)
    
    var startPointX: CGFloat = 0
    var startPointY: CGFloat = 0
    var endPointX: CGFloat = 1
    var endPointY: CGFloat = 1
    
    override func layoutSubviews() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: startPointX, y: startPointY)
        gradientLayer.endPoint = CGPoint(x:endPointX, y:endPointY)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

